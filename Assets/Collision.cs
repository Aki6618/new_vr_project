﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Collision : MonoBehaviour {

    private void OnCollisionEnter(UnityEngine.Collision other)
    {
        if(other.gameObject.name == "Cube1")
        {
            Destroy(this.gameObject);
        }

    }
}
