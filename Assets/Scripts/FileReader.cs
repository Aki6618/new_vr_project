﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;
using System.IO;

namespace vr {
    public class FileReader : MonoBehaviour {

        static readonly string filepath = "/Documents/";


        // Use this for initialization
        void Start() {

        }

        // Update is called once per frame
        void Update() {

        }

        public Molecule DecodeFile(string filename) {
            Molecule mol;
            mol.atoms = new List<Atom>();
            string line;
            using (StreamReader sr = new StreamReader(Application.dataPath + filepath + filename)) {
                sr.ReadLine();
                sr.ReadLine();
                while (true) {
                    line = sr.ReadLine();
                    if (line == null) break;
                    string[] adata;
                    adata = line.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);
                    Debug.Log(filename + " " + adata.Length);
                    if (adata.Length < 4) break;
                    Atom atom = new Atom();
                    atom.type = adata[0];
                    atom.pos = new Vector3(float.Parse(adata[1]), float.Parse(adata[2]), float.Parse(adata[3]));
                    mol.atoms.Add(atom);
                }
                
            }
            mol.bonds = new List<int>();
            for(int i = 0; i < mol.atoms.Count; i++) {
                for(int j = i + 1; j < mol.atoms.Count; j++) {
                    if(JudgeAtomsDistance(mol.atoms[i], mol.atoms[j])) {
                        mol.bonds.Add(i);
                        mol.bonds.Add(j);
                    }
                }
            }

            return mol;
        }


        bool JudgeAtomsDistance(Atom a, Atom b) {
            return (a.pos - b.pos).magnitude <= AtomData.atomBonds[a.type] + AtomData.atomBonds[b.type];
        }

    }
}


