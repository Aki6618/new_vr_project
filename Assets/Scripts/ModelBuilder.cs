﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace vr {

    public struct Atom {
        public string type;
        public Vector3 pos;
    }

    public struct Molecule {
        public List<Atom> atoms;
        public List<int> bonds;
    }

    public class ModelBuilder : MonoBehaviour {

        public FileReader fr;
        public string filename;
        public GameObject container_obj, atom_obj;

        public Material mat;


        // Use this for initialization
        void Start() {
            Molecule mol = fr.DecodeFile(filename);

            BuildModel(mol);
        }

        // Update is called once per frame
        void Update() {

        }

        void BuildModel(Molecule mol) {
            GameObject container = Instantiate(container_obj);
            foreach(var atom in mol.atoms) {
                GameObject obj = Instantiate(atom_obj, container.transform);
                obj.transform.localPosition = atom.pos;
                obj.transform.localScale *= AtomData.atomSizes[atom.type];
                obj.GetComponent<MeshRenderer>().material = mat;
                obj.GetComponent<MeshRenderer>().material.color = AtomData.atomColors[atom.type];
                var label = obj.transform.GetChild(0).gameObject;
                label.GetComponent<AtomText>().radius = AtomData.atomSizes[atom.type];
                label.GetComponent<TextMesh>().text = atom.type;
            }

            for (int i = 0; i < mol.bonds.Count; i += 2) {
                BuildBonds(mol.atoms[mol.bonds[i]], mol.atoms[mol.bonds[i + 1]], container);
            }

            container.transform.position = transform.position;
            container.transform.localScale *= 0.1f;


        }
        
        void BuildBonds(Atom a, Atom b, GameObject container) {
            GameObject obj = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
            obj.transform.parent = container.transform;
            obj.transform.localPosition = (a.pos + b.pos) / 2;
            obj.transform.forward = a.pos - b.pos;
            obj.transform.Rotate(Vector3.right * 90);
            obj.transform.localScale = new Vector3(0.2f, (a.pos - b.pos).magnitude / 2, 0.2f);
            obj.GetComponent<MeshRenderer>().material = mat;
            obj.GetComponent<MeshRenderer>().material.color = new Color(0.5f, 0.5f, 0.5f);
        }
    }



}

