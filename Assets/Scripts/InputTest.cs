﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using HoloToolkit.Unity.InputModule;


public class InputTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
	}


	// Update is called once per frame
	void Update () {
        Vector3 v = Vector3.right * Input.GetAxis("VR_rightHorizontal") + Vector3.up * Input.GetAxis("VR_rightVertical");
        Vector3 r = Vector3.right * Input.GetAxis("VR_leftHorizontal") + Vector3.up * Input.GetAxis("VR_leftVertical");
        Debug.Log(v + " " + r);
        transform.position += v * 0.01f;
        transform.Rotate(r);
    }
}
