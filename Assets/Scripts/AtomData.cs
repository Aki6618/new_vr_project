﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AtomData {

    public static Dictionary<string, float> atomSizes = new Dictionary<string, float>{
        { "H", 0.53f }, { "O", 0.48f }, { "C", 0.67f }, { "N", 0.56f }, { "F", 0.42f }
    }; 
    public static Dictionary<string, float> atomBonds = new Dictionary<string, float>{
        { "H", 0.37f }, { "O", 0.73f }, { "C", 0.77f }, { "N", 0.75f }, { "F", 0.71f }
    };
    public static Dictionary<string, Color> atomColors = new Dictionary<string, Color>{
        { "H", new Color(1, 1, 1) }, { "O", new Color(1, 0, 0) }, { "C", new Color(0, 0, 0) }, { "N",  new Color(0, 0, 1) }, { "F",  new Color(0, 1, 0) }
    };


}
