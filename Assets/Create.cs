﻿using UnityEngine;
using VRTK;

public class Create : MonoBehaviour
{
    public GameObject target;
    private void OnEnable()
    {
        if (GetComponent<VRTK_ControllerEvents>() == null)
            return;
        // イベントハンドラの登録
        GetComponent<VRTK_ControllerEvents>().TriggerPressed += TriggerPressedHandler;
    }

    private void OnDisable()
    {
        if (GetComponent<VRTK_ControllerEvents>() == null)
            return;
        // イベントハンドラの解除
        GetComponent<VRTK_ControllerEvents>().TriggerPressed -= TriggerPressedHandler;
    }

    // イベントハンドラ
    private void TriggerPressedHandler(object sender, ControllerInteractionEventArgs e)
    {
        float x = Random.Range(0.0f, 2.0f);
        float z = Random.Range(0.0f, 2.0f);
        Instantiate(target, new Vector3(x, 2.0f, z), Quaternion.identity);
    }
}
